﻿angular.module('controllers').
controller('CongratulationController', ['RegistrationService', '$scope', '$rootScope', '$location',  function (registrationService, $scope, $rootScope, $location) {
    var self = this;
    $scope.errors = [];

    // EVENTS
    $scope.$on('$viewContentLoaded', function () {
        $scope.user = registrationService.getUserInfo();
        if (!$scope.user) {
            $location.path("/");
            return;
        }

        $scope.contactViaList = registrationService.contactViaList;
        $scope.user.contactVia = registrationService.getContactViaById($scope.user.contactViaId);

        $scope.locations = registrationService.locations;
        $scope.user.location = registrationService.getLocationById($scope.user.locationId);

        $scope.technologies = registrationService.technologies;
        $scope.languages = registrationService.languages;
        registrationService.getLanguageLevels($scope.user.languages);

        $scope.languageLevels = registrationService.languageLevels;
    });
}]);