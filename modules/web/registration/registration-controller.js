﻿angular.module('controllers').
controller('RegistrationController', ['RegistrationService', '$scope', '$rootScope', function (registrationService, $scope, $rootScope) {
    var self = this;
    $scope.errors = [];
    $scope.user = {};

    $scope.isContactInformationTabActive = true;
    $scope.isSkillsTabVisibleActive = false;

    $scope.isFirstStageValid = false;
    $scope.isSecondStageValid = false;

    // default values
    $scope.user.contactViaId = "1";
    $scope.user.locationId = "-1";
    $scope.userDefaultLanguageLevelId = "1";
    //$scope.user.technologies = [];


    $scope.next = function () {
        $scope.isFirstStageValid = registrationService.next($scope.user);
        if ($scope.isFirstStageValid) {
            $('a[href="#skills"]').tab('show');
        }
    };

    $scope.submit = function () {
        if (!registrationService.isFirstStageValid($scope.user)) {
            $('a[href="#contact-information"]').tab('show');
            return;
        }
        registrationService.submit($scope.user);
    };

    $scope.languageIsSelected = function (language) {
        return _.contains($scope.user.languages, language)
    };

    $rootScope.$on('register:errors', function (event, args) {
        $scope.errorList = args;
        $scope.$evalAsync();
    });


    self.initElements = function () {
        $('.phone').mask('000-000-0000');
    };

    self.initTabs = function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });
        $('.nav-tabs a').on('shown.bs.tab', function (event) {
            var x = $(event.target).text();         // active tab
            var y = $(event.relatedTarget).text();  // previous tab
            $(".act span").text(x);
            $(".prev span").text(y);
        });
    };




    self.bindEvents = function () {
        $('#register-form-wrapper').find('input').on('keyup', function (e) {
            if (e.keyCode == 13) {
                $scope.register();
            }
        });
    };

    self.unBindEvents = function () {
        $('#register-form-wrapper').find('input').off('keyup');
    };

    $scope.$on('$destroy', function () {
        self.unBindEvents();
    });


    // EVENTS
    $scope.$on('$viewContentLoaded', function () {
        self.initElements();
        self.initTabs();

        $scope.contactViaList = registrationService.contactViaList;
        $scope.locations = registrationService.locations;
        $scope.technologies = registrationService.technologies;
        $scope.languages = registrationService.languages;
        $scope.languageLevels = registrationService.languageLevels;
    });

    $rootScope.$on('register:errors', function (event, args) {
        $scope.errors = args
    });

}]);