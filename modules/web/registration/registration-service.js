﻿angular.module('services').
service('RegistrationService', ['RegistrationDataFactory', '$window', '$location', '$rootScope',
function(registrationDataFactory, $window, $location, $rootScope) {
    var self = this;

    self.contactViaList = [
        { id: 1, name: 'Email' },
        { id: 2, name: 'Skype' },
        { id: 3, name: 'Phone (number should have xxx-xxx-xxxx format)' },
    ];

    self.locations = [
        { id: 1, name: "USA" },
        { id: 2, name: "Canada" },
        { id: 3, name: "Ukraine" },
        { id: 4, name: "Scotland" }
    ];

    self.technologies = [
        { id: 1, name: ".NET" },
        { id: 2, name: "HTML" },
        { id: 3, name: "Java" },
        { id: 4, name: "Javascript" },
        { id: 5, name: "CSS" },
        { id: 6, name: "AngularJS" },
        { id: 7, name: "Backbone.js" },
        { id: 8, name: "Entity Framework" },
        { id: 9, name: "Linq2SQL" }
    ];

    self.languages = [
        { id: 1, name: "English", languageLevelId: "-1" },
        { id: 2, name: "Chinese", languageLevelId: "-1" },
        { id: 3, name: "Russian", languageLevelId: "-1" },
        { id: 4, name: "Ukranian", languageLevelId: "-1" },
        { id: 5, name: "Polish", languageLevelId: "-1" }
    ];

    self.languageLevels = [
        { id: 1, name: "Basic" },
        { id: 2, name: "Independent" },
        { id: 3, name: "Proficient" },
    ];

    this.register = function(user) {
        if (!self.isValid(user)) return;
        registrationDataFactory.register(user).success(function(response) {
            if (response) {
                Notification.showMessage('notice', 'User was registered sucessfully!');
                $location.path("#/");
            } else {
                
            }
        }.bind(this));
    };


    self.next = function (user) {
        return self.isFirstStageValid(user);
    };

    self.submit = function (user) {
        var isValid = self.isSecondStageValid(user);
        if (!isValid) return false;

        /*
        registrationDataFactory.register(user).success(function (response) {
            if (response) {
                // success message and redirect to user info
                $location.path("#/congratulation");
            } else {
                // error message
            }
        }.bind(this));
        */

        // ... redirect to user info
        self.setUserInfo(user);
        $location.path("/congratulation");
    };

    self.setUserInfo = function (user) {
        self.user = user;
    };

    self.getUserInfo = function () {
        return self.user;
    };


    // validation
    self.isFirstStageValid = function(user) {
        var errorList = [];

        // first name
        if (!user.firstName) {
            errorList.push('First name is required');
        } else {
            if (user.firstName.length < 3) errorList.push('First name should have at least 3 characters');
            if (user.firstName.length > 50) errorList.push('First name should have no more than 50 characters');
        }

        // last name
        if (!user.lastName) {
            errorList.push('Last name is required');
        } else {
            if (user.lastName.length < 3) errorList.push('Last name should have at least 3 characters');
            if (user.lastName.length > 50) errorList.push('Last name should have no more than 50 characters');
        }

        // contacts
        // email
        if (user.contactViaId == 1 && !user.email) {
            errorList.push('Email is required');
        } else if (user.contactViaId == 1 && user.email) {
            if (!self.validateEmail(user.email)) errorList.push('E-mail has wrong format');
            if (user.email.length > 100) errorList.push('Email should should have no more than 100 characters');
        }

        // skype
        if (user.contactViaId == 2 && !user.skype) {
            errorList.push('Skype is required');
        }

        // phone
        if (user.contactViaId == 3 && !user.phone) {
            errorList.push('Phone number is required');
        } else if (user.contactViaId == 3 && user.phone) {
            var phone = user.phone.replace('-', '');
            if (user.phone.length < 10) errorList.push('Phone number has wrong format');
        }
            
        $rootScope.$broadcast("register:errors", errorList);
        return !errorList.length;
    };

    self.isSecondStageValid = function (user) {
        var errorList = [];

        // technologies
        if (!user.technologies || !user.technologies.length) {
            errorList.push('Please select technologies');
        }

        // languages
        if (!user.languages || !user.languages.length) {
            errorList.push('Please select languages');
        } else if (user.languages.length) {
            _.each(user.languages, function (language) {
                if (language.languageLevelId == -1) {
                    errorList.push('Please select "' + language.name + '" language level');
                }     
            });
        }

        $rootScope.$broadcast("register:errors", errorList);
        return !errorList.length;
    };

    self.validateEmail = function(email) {
        var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return regex.test(email);
    };

    // service
    self.getLocationById = function (id) {
        return _.findWhere(self.locations, { id: +id });
    };

    self.getContactViaById = function (id) {
        return _.findWhere(self.contactViaList, { id: +id });
    };

    self.getLanguageLevels = function (languages) {
        _.each(languages, function (language) {
            language.languageLevel = _.findWhere(self.languageLevels, { id: +language.languageLevelId });
        });
    };
}]);