﻿var app = angular.module('app', [
    'controllers',
    'services',
    'factories',
    'ngRoute',
    'ui.router',
    "checklist-model"
]);

angular.module('factories', []);
angular.module('services', ['factories']);
angular.module('controllers', ['services', 'factories']);