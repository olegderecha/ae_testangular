﻿/// <reference path="modules/web/registration/views/registration.html" />
app.config(['$routeProvider', '$stateProvider', '$urlRouterProvider',
function($routeProvider, $stateProvider, $urlRouterProvider) {
    var baseWebUrl = "modules/web/";

    $stateProvider
        
        .state('default', {
            url: '/',
            views: {
                'main': {
                    templateUrl: baseWebUrl + 'registration/views/registration.html',
                    controller: 'RegistrationController'
                }
            }
        })

        .state('congratulation', {
            url: '/congratulation',
            views: {
                'main': {
                    templateUrl: baseWebUrl + 'congratulation/views/congratulation.html',
                    controller: 'CongratulationController'
                }
            }
        })
    ;
    
    $urlRouterProvider.otherwise('/');
}]);